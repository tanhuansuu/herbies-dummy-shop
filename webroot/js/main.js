$(document).on("click", ".modal-dec", function(){

	if(typeof $(this).attr('data-checkout') !== "undefined"){
		var input = $(this).closest("td").find(".modal-input");
	}else{
		var input = $(".modal-input");
	}

	var val = parseInt(input.val()) - 1;
	input.val(val < 0 ? 0 : val);
	input.attr("value", val);

});

$(document).on("click", ".modal-inc", function(){

	if(typeof $(this).attr('data-checkout') !== "undefined"){
		var input = $(this).closest("td").find(".modal-input");
	}else{
		var input = $(".modal-input");
	}

	var val = parseInt(input.val()) + 1;
	input.val(val);	
	input.attr("value", val);

});

$(document).on("click", ".filter-expander", function(){
	$(".filter-box").toggle();
});

$(document).on("click", ".card-product", function(){

	$(".modal-input").val(0);

	var name = $(this).attr("data-name");
	var image = $(this).attr("data-image");
	var description = $(this).attr("data-description");
	var price = $(this).attr("data-price");
	var id = $(this).attr("data-id");
	var sku = $(this).attr("data-sku");

	$(".modal-sku").html(sku);
	$(".modal-title").html(name + " (" + sku + ")" );
	$(".modal-price").html(price);
	$(".modal-description").html(description);
	$(".modal-image").attr("src", image);
	$("#input-product-id").val(id);

});

$(document).on("click", ".cart-container", function(){
	var url = $(this).attr("data-action");

	window.location.href = url;
});


$(document).on("click", ".modal-add-to", function(){

	var url = $(this).attr("data-action");

	if(typeof $(this).attr('data-checkout') !== "undefined"){
		var value = $(this).closest("td").find(".modal-input").val();
		var id = $(this).closest("td").find(".hidden-product-id").val();
	}else{
		var value = $(".modal-input").val();
		var id = $("#input-product-id").val();		
	}

	var direct = false;
	if(typeof $(this).attr('data-checkout') !== "undefined"){
		direct = true;
	}

	$.ajax({
		method: "POST",
	    headers: {
	        'X-CSRF-Token': csrfToken
	    },
		url: url,
		data: { id: id, quantity: value, directUpdate: direct },
		success: function(result){

			var totalQuantity = 0;
			var totalAmount = 0;
			for (var key in result){
				totalQuantity += parseInt(result[key]["quantity"]);
				totalAmount += parseFloat(result[key]["price"]) * parseInt(result[key]["quantity"]);
			}

			$(".cart-item-count").html(totalQuantity);
			if($(".checkout-total").length > 0){
				$(".checkout-total").html("<b>Total Price: </b>" + totalAmount);
			}
		}
	})
});

$(document).on("click", ".checkout-remove-item", function(){

	var _this = this;
	var url = $(this).attr("data-action");
	var id = $(this).closest("td").find(".hidden-product-id").val();

	$.ajax({
		method: "DELETE",
	    headers: {
	        'X-CSRF-Token': csrfToken
	    },
		url: url,
		data: { id: id, removeItem: true },
		success: function(result){

			var totalQuantity = 0;
			var totalAmount = 0;
			for (var key in result){
				totalQuantity += parseInt(result[key]["quantity"]);
				totalAmount += parseFloat(result[key]["price"]) * parseInt(result[key]["quantity"]);
			}

			$(".cart-item-count").html(totalQuantity);
			$(_this).closest("tr").remove();
			if($(".checkout-total").length > 0){
				$(".checkout-total").html("<b>Total Price: </b>" + totalAmount);
			}
		}
	})
});

function submitOrder(event){
	event.preventDefault();

	var url = $(".submitOrder").attr("data-action");

	$.ajax({
		method: "DELETE",
	    headers: {
	        'X-CSRF-Token': csrfToken
	    },
		url: url,
		data: { placeOrder: true },
		success: function(result){

			var totalQuantity = 0;
			$(".cart-item-count").html(totalQuantity);
			$(".checkout-container").html('<div class="alert alert-success" role="alert">Thank you for your order!</div>');
		}
	});
}

function sortProducts(sortBy, sortDir){

	var sortedCards = $('.card').sort(function (a, b) {
		var contentA = $(a).attr(sortBy);
		var contentB = $(b).attr(sortBy);

		if(sortDir === "ASC" && sortBy === "data-name"){
			return contentA.localeCompare(contentB);
		}else if(sortBy === "data-name"){
			return contentB.localeCompare(contentA);			
		}else if(sortDir === "LOW" && sortBy === "data-price"){
			return parseFloat(contentA) - parseFloat(contentB);
		}else{
			return parseFloat(contentB) - parseFloat(contentA);
		}
	})

	$(".card-container").html(sortedCards);
	$(".filter-box").toggle();
}

$(document).on("click", ".navbar-search-action", function(){
	var searchParam = $(".navbar-search").val();
	var url = $(this).attr("data-action");

	window.location.href = url+"?filter="+searchParam;
});

$(document).on("keyup", '.navbar-search', function(e){
	if (e.keyCode == 13) {
		$(".navbar-search-action").click();
	}
})