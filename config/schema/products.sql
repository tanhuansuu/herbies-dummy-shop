-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2019 at 08:26 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbies`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sku`, `description`, `price`, `image`, `created`, `modified`) VALUES
(4, 'Salt', 'salt-1', 'A usually whitish crystalline solid, chiefly sodium chloride, used extensively in ground or granulated form as a food seasoning and preservative.', '10', 'uploads/images/d74c413515.jpg', '2019-06-23 10:24:43', '2019-06-23 11:53:17'),
(5, 'Black Pepper', 'pepper-1', 'Black pepper, Piper nigrum, is a climbing perennial plant in the family Piperaceae which is grown for its fruits.', '5', 'uploads/images/642fd0bcb3.jpg', '2019-06-23 10:25:14', '2019-06-23 10:25:14'),
(6, 'Cinnamon', 'cinnamon-1', 'The sweet-spicy flavor of Cinnamon enhances the taste of vegetables and fruits.', '26', 'uploads/images/62456ac8f3.jpg', '2019-06-23 10:25:49', '2019-06-23 10:25:49'),
(7, 'Basil', 'basil-1', 'Basil is likely native to India and is widely grown as a kitchen herb.', '6', 'uploads/images/541f5c34ca.jpg', '2019-06-23 10:26:57', '2019-06-23 11:52:47'),
(9, 'Parsley', 'parsley', 'Parsley, Petroselinum crispum is an herbaceous biennial or perennial plant in the family Apiaceae grown for its leaves which are used as a herb. Parsley is an aromatic plant with an erect growth habit and possesses branched, hollow stems and dark green flat or curled leaves which are arranged alternately on the stems.', '6', 'uploads/images/8007160ef7.jpg', '2019-06-23 17:26:51', '2019-06-23 17:27:22'),
(10, 'Garlic', 'garlic-1', 'Garlic (Allium sativa ), is a plant with long, flat grasslike leaves and a papery hood around the flowers. ... The stalk rises directly from the flower bulb, which is the part of the plant used as food and medicine. The bulb is made up of many smaller bulbs covered with a papery skin known as cloves.', '1', 'uploads/images/0b928e44db.jpg', '2019-06-23 17:27:58', '2019-06-23 17:27:58'),
(11, 'Oregano', 'oregano-1', 'Oregano is related to the herb marjoram, sometimes being referred to as wild marjoram. Oregano has purple flowers and spade-shaped, olive-green leaves. It is a perennial,[4][5] although it is grown as an annual in colder climates, as it often does not survive the winter.[6][7] Oregano is planted in early spring, the plants being spaced 30 cm (12 in) apart in fairly dry soil, with full sun. Oregano will grow in a pH range between 6.0 (mildly acidic) and 9.0 (strongly alkaline), with a preferred range between 6.0 and 8.0. It prefers a hot, relatively dry climate, but does well in other environments', '25', 'uploads/images/6223b56bec.jpg', '2019-06-23 17:28:52', '2019-06-23 17:28:52'),
(12, 'Mint', 'mint-1', 'Mint plants are mainly aromatic perennials and they possess erect, branching stems and oblong to ovate or lanceolate leaves arranged in opposing pairs on the stems. The leaves are often covered in tiny hairs and have a serrated margin. ... Mint plants are fast growing and can become very invasive.', '2', 'uploads/images/50ef4c44bf.jpg', '2019-06-23 17:29:42', '2019-06-23 17:29:42'),
(13, 'White Pepper', 'pepper-2', 'White Pepper. ... They have a milder flavor than the less ripe version (black peppercorns). White pepper is used as an alternative to black pepper in recipes such as white sauces or white mashed potatoes when a more pleasing appearance is desired to eliminate the black specks throughout the food.', '66', 'uploads/images/9905b24776.jpg', '2019-06-23 17:34:04', '2019-06-23 17:34:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
