###SETUP###

1. Composer install
2. Set database connection config/app.php
3. Import schemas into database from config/schema/*
4. Npm install

5. bin/cake server -p 8765 (or setup into xampp or similar)