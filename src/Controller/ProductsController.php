<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Cart');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $products = $this->paginate($this->Products);

        $filterBy = $this->request->getQuery('filter');
        if(isset($filterBy)){

            $products = $this->Products->find('all', array(
                'conditions' => array(
                    'OR' => array(
                        'sku LIKE' => '%'.$filterBy.'%',
                        'name LIKE' => '%'.$filterBy.'%',
                        'description LIKE' => '%'.$filterBy.'%'
                    )
                )
            ))->toArray();

            $this->set('products', $products);

        }else{

            $this->set(compact('products'));
        }

    }

    public function checkout(){

        $currentCart = $this->Cart->getCart();

        $this->set(compact('product'));
    }

    public function addToCart(){

        $data = $this->request->getData();

        if ($this->request->is('post')) {
            

            if(isset($data["id"]) && isset($data["quantity"])){
                $id = $data["id"];
                $quantity = $data["quantity"];
                $directUpdate = false;
                if(isset($data["directUpdate"])){
                    $directUpdate = filter_var($data["directUpdate"], FILTER_VALIDATE_BOOLEAN); 
                }

                $currentCart = $this->Cart->addToCart($id, $quantity, $directUpdate);

                return $this->response->withType("application/json")->withStringBody(json_encode($currentCart));
            }
        }else if($this->request->is('delete')){

            if(isset($data["placeOrder"])){

                $currentCart = $this->Cart->refreshCart();
                return $this->response->withType("application/json")->withStringBody(json_encode($currentCart));

            }else{
                
                if(isset($data["id"])){
                    $currentCart = $this->Cart->removeFromCart($data["id"]);

                    return $this->response->withType("application/json")->withStringBody(json_encode($currentCart));
                }               
            }
        }
    }
}
