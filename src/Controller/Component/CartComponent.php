<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class CartComponent extends Component
{

	public function getCart(){
		return $this->request->session()->read("CurrentCart");
	}

	public function refreshCart(){
		return $this->request->session()->delete("CurrentCart");
	}

	public function addToCart($id, $quantity = 1, $directUpdate = false){

		$currentCart = $this->getCart();

		$controller = $this->_registry->getController();
        if(!is_numeric($quantity) || $quantity < 1) {
            $quantity = 0;
        }

        $quantity = abs($quantity);

        $product = $controller->Products->get($id, [
            'contain' => []
        ]);

        $updatedQuantity = $quantity;
        if(isset($currentCart[$id])){

        	if(!$directUpdate){
        		$updatedQuantity = $quantity + $currentCart[$id]["quantity"];
        	}else{
        		$updatedQuantity = $quantity;
        	}
        }

        $data = [
        	"product_id" => $product->id,
        	"quantity" => $updatedQuantity,
        	"name" => $product->name,
        	"sku" => $product->sku,
        	"image" => $product->image,
        	"price" => $product->price,
        	"description" => $product->description
        ];

        $this->request->session()->write("CurrentCart.".$id, $data);

        return $this->getCart();
	}

	public function removeFromCart($id){
		if(isset($this->getCart()[$id])){
			$this->request->session()->delete("CurrentCart.".$id);
		}

		return $this->getCart();
	}

}