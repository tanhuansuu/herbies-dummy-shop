<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
?>
<div class="container container--fixed">
        <div class="col-md-12">
            <div class="table-actions"><?= $this->Html->link(__('New Product'), ['action' => 'add'], ['class' => 'table-add-product']) ?></div>
        </div>

        <div class="col-md-12">
            <div class="admin-product-container admin-product-container-list">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('image') ?></th>

                            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('sku') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                            
                            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product): ?>
                        <tr>
                            <td><?= $this->Number->format($product->id) ?></td>
                            <td><img class="table-image" src="<?php echo $this->request->webroot.h($product->image); ?>" /></td>
                            <td><?= h($product->name) ?></td>
                            <td><?= h($product->sku) ?></td>
                            <td><?= $this->Number->format($product->price) ?></td>
                            
                            <td><?= h($product->created) ?></td>
                            <td><?= h($product->modified) ?></td>
                            <td class="actions">

                                <button class="card-product table-view-btn" data-toggle="modal" data-target="#product-view" 
                                    data-name="<?= h($product->name) ?>" 
                                    data-image="<?php echo $this->request->webroot.h($product->image); ?>"
                                    data-description="<?= h($product->description) ?>"
                                    data-price="<?= $this->Number->format($product->price) ?>€ /kg">
                                    View
                                </button>

                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete {0}?', $product->name)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
        </div>
</div>

<div class="modal fade" id="product-view" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productModalLabel">Salt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="modal-image" alt="product-image" src="" />
                    </div>
                    <div class="col-md-12">
                        <div class="modal-price"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="modal-description"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="modal-close-btn" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>