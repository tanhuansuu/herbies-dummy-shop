<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class="container container--fixed">
    <div class="col-md-12">
        <div class="table-actions">
            <?= $this->Form->postLink(
                    __('Delete'),
                    ['action' => 'delete', $product->id],
                    ['confirm' => __('Are you sure you want to delete {0}?', $product->name)]
                )
            ?> 
            <?= $this->Html->link(__('List Products'), ['action' => 'index']) ?>          
        </div>
        </div>
        <div class="col-md-12">
            <div class="admin-product-container admin-product-container-modify">
            <?= $this->Form->create($product, array("type" => "file")) ?>
            <fieldset>
                <legend><?= __('Edit Product') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('sku');
                    echo $this->Form->control('description');
                    echo $this->Form->control('price');
                    echo $this->Form->control('image', array("type" => "file", "required" => false));
                ?>

                <h6 class="edit-image-current">Current image</h6>
                <img class="edit-image" src="<?php echo $this->request->webroot.h($product->image); ?>" />

            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>