<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<div class="container container--fixed">
    <div class="col-md-12">
        <div class="table-actions">
            <?= $this->Html->link(__('List Products'), ['action' => 'index']) ?>          
        </div>
        </div>
        <div class="col-md-12">
            <div class="admin-product-container admin-product-container-modify">
            <?= $this->Form->create($product, array("type" => "file")) ?>
            <fieldset>
                <legend><?= __('Add Product') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('sku');
                    echo $this->Form->control('description');
                    echo $this->Form->control('price');
                    echo $this->Form->control('image', array("type" => "file"));
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
