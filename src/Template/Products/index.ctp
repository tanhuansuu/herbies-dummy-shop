<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
*/
?>
<div class="container container--fixed">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-container">
                <div class="filter-expander">
                    SORT BY: 
                </div>
                <div class="filter-box">
                    <div onclick="sortProducts('data-name', 'ASC')">Name - ASC</div>
                    <div onclick="sortProducts('data-name', 'DESC')">Name - DESC</div>
                    <div onclick="sortProducts('data-price', 'HIGH')">Price - High-Low</div>
                    <div onclick="sortProducts('data-price', 'LOW')">Price - Low-High</div>
                </div>
            </div>
        </div>
        <div class="col-md-12">

            <div class="card-container">

                <?php foreach ($products as $product): ?>

                    <div class="card card-product" data-toggle="modal" data-target="#product-view" 
                        data-id="<?= h($product->id) ?>"
                        data-name="<?= h($product->name) ?>" 
                        data-sku="<?= h($product->sku) ?>"
                        data-image="<?php echo $this->request->webroot.h($product->image); ?>"
                        data-description="<?= h($product->description) ?>"
                        data-price="<?= $this->Number->format($product->price) ?>€ /kg" >

                        <img class="card-img-top" src="<?php echo $this->request->webroot.h($product->image); ?>" />
                        <div class="card-body">
                            <h5 class="card-title"><?= h($product->name) ?></h5>
                            <p class="card-text"><?= mb_strimwidth(h($product->description),0,75,"...") ?></p>

                            <div class="card-bottom"><?= $this->Number->format($product->price) ?>€ /kg</div>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="product-view" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productModalLabel">Salt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="modal-image" alt="product-image" src="" />
                    </div>
                    <div class="col-md-12">
                        <div class="modal-price"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="modal-description"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="modal-amount-btn modal-dec" type="button">-</button>
                <input class="modal-input" type="number" value="0"  />
                <input id="input-product-id" value="<?= h($product->id) ?>" type="hidden" />
                <button class="modal-amount-btn modal-inc" type="button">+</button>
                <button class="modal-add-to" data-dismiss="modal" type="button" data-action="<?= $this->Url->build(['action' => 'addToCart']) ?>">Add to cart</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>
    </script>
</div>
