<div class="container container--fixed">
    <div class="row checkout-container">
        <div class="col-md-8">
        	<div class="admin-product-container admin-product-container-list">
        		<?php if($this->request->session()->read("CurrentCart")) : ?>
	                <table class="table">
	                    <tbody>
	                    	<?php $checkoutTotal = 0; ?>
		                        <?php foreach ($this->request->session()->read("CurrentCart") as $item): ?>
		                        <?php $checkoutTotal += ($item["quantity"] * $item["price"] ); ?>
		                        <tr>
		                            <td><img class="table-image" src='<?= $item["image"]; ?>' /></td>
		                            <td><?= $item["name"]; ?></td>
		                            <td>
		                            	<button class="modal-amount-btn modal-dec" type="button" data-checkout="true">-</button>
		                            	<input class="modal-input" type="number" value='<?= $item["quantity"]; ?>'  />
		                            	<input class="hidden-product-id" id="input-product-id" value='<?= $item["product_id"]; ?>' type="hidden" />
		                            	<button class="modal-amount-btn modal-inc" type="button" data-checkout="true">+</button>
		                            	<button class="modal-add-to" type="button" data-checkout="true" data-action="<?= $this->Url->build(['action' => 'addToCart']) ?>" data-direct="true">Update</button>

		                            	<button class="red-btn checkout-remove-item" type="button" data-action="<?= $this->Url->build(['action' => 'addToCart']) ?>">Remove</button>
		                            </td>
		                        </tr>
		                        <?php endforeach; ?>
	                    </tbody>
	                </table>
	                
	                <hr />
	                <div class="checkout-total"><b>Total Price: </b><?= $checkoutTotal; ?> €</div>
                <?php else: ?>
                	<div class="no-products-in-cart">You don't have any product yet in your cart.</div>
                <?php endif; ?>
        	</div>
        </div>
		<div class="col-md-4">
			<div class="background-card">
		      	<h4 class="mb-3">Name & Address</h4>
		      	<form onSubmit="submitOrder(event)" data-action="<?= $this->Url->build(['action' => 'addToCart']) ?>" class="submitOrder">
		        	<div class="row">
			          	<div class="col-md-12 mb-3">
			            	<label for="firstName">Name</label>
			            	<input type="text" class="form-control" id="name" placeholder="" value="" required="true">
			          	</div>
			          	<div class="col-md-12 mb-3">
			          		<label for="address">Address</label>
			          		<textarea class="form-control" id="address" required="true"></textarea>          	
			          	</div>
		        	</div>
		        	<button class="green-btn" type="submit">Place order</button>
		      </form>
	  		</div>
	    </div>
    </div>
</div>
<script type="text/javascript">
	var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>
</script>