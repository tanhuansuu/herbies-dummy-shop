<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<div class="welcome-box">
    <div class="welcome-box--small">DISCOVER FLAVORS</div>
    <div class="welcome-box--main">SUMMER 2019</div>
    <a type="button" class="btn welcome-box--button" href="<?= $this->Url->build("shop", ["controller" => "Products", "action" => "index"]) ?>">Enter Shop</a>
</div>