<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Herbies
    </title>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap&subset=latin-ext" rel="stylesheet">

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('main.css') ?>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <?= $this->Html->script("main.js") ?>

</head>
<body>
    <nav class="navbar navbar-light justify-content-between navbar-customized">
        <a class="navbar-brand" href="<?= $this->Url->build("shop", ["controller" => "Products", "action" => "index"]) ?>">
            <?= $this->Html->image('logo.png', [
                "alt" => "herbies-logo",
                "height" => 50
            ]
            ) ?>
        </a>
        <div class="navbar-right">
            <input class="navbar-search" type="text" placeholder="Search" />
            <i class="fas fa-search navbar-search-action" data-action="<?= $this->Url->build("shop", ["controller" => "Products", "action" => "index"]) ?>" ></i>
            <div class="cart-container" data-action="<?= $this->Url->build("checkout", ["controller" => "Products", "action" => "checkout"]) ?>">
                <i class="fas fa-shopping-basket"></i>
                <?php if($this->request->session()->read("CurrentCart")) : ?>
                    <?php 
                        $totalQuantity = 0;
                    ?>
                    <?php foreach ($this->request->session()->read("CurrentCart") as $item): ?>
                        <?php $totalQuantity += $item["quantity"]; ?>
                    <?php endforeach; ?>

                    <div class="cart-item-count"><?php echo $totalQuantity; ?></div>
                <?php else :  ?> 
                    <div class="cart-item-count">0</div>
                <?php endif; ?> 
            </div>
        </div>
    </nav>

    <main>
        <?= $this->fetch('content') ?>
    </main>

</body>
</html>
